#include <ESP8266WiFi.h>
#include <ArduinoJson.h>


struct Device {
  int id;
  char name[32];
  char mac[45];
  char ip[32];
  char description[90];
};

struct Component {
  int id;
  int deviceID;
  char name[32];
  char description[90];
  int pin;
};

struct Property {
  int id;
  int componentID;
  char name[32];
  char mode[4];
  char value[20];
};

/* Internet properties */
const char* ssid     = ""; //your Wifi ssid
const char* password = ""; //your Wifi password
byte mac[6]; // the MAC address of your Wifi shield
// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(80);

/* Multidevices system */
const size_t MAX_CONTENT_SIZE = 512;       // max size of the HTTP response
const char* host = "192.168.0.100";
const char* prefix = "/MultiDevicesREST/api/device/";
const char* device_name = "Wemos D1";
const char* device_description = "Wemos for test purposes";
String localIp;
const int COMPONENTS_SIZE = 1;
const int PROPERTIES_SIZE = 1;
Device device;
Component components[COMPONENTS_SIZE];
Property properties[COMPONENTS_SIZE * PROPERTIES_SIZE];

/* State variables */
bool deviceAdded = false;
bool componentsAdded = false;
bool propertiesAdded = false;
bool firstTime = true;

bool timeout(WiFiClient client) {
  int timeout = millis() + 5000;
  while (client.available() == 0) {
    if (timeout - millis() < 0) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return true;
    }
  }
  return false;
}

bool deleteRequest(String url, WiFiClient client) {
  client.print("DELETE ");
  client.print(url);
  client.println(" HTTP/1.1");
  client.print("Host: ");
  client.print(host);
  client.println(":8080");
  client.print("Connection: ");
  client.println("keep-alive");
  client.print("Cache-Control: ");
  client.println("no-cache");
  client.println();
  return true;
}

bool postRequest(String url, String json, WiFiClient client) {
  client.print("POST ");
  client.print(url);
  client.println(" HTTP/1.1");
  client.print("Host: ");
  client.print(host);
  client.println(":8080");
  client.print("Connection: ");
  client.println("keep-alive");
  client.print("Content-Length: ");
  client.println(json.length());
  client.print("Content-Type: ");
  client.println("application/json");
  client.print("Cache-Control: ");
  client.println("no-cache");
  client.println();
  client.println(json);

  return true;
}

/*
   INITIALIZATION
*/

void initDevice() {
  String macstr = String(mac[5], HEX);
  macstr += ":";
  macstr += String(mac[4], HEX);
  macstr += ":";
  macstr += String(mac[3], HEX);
  macstr += ":";
  macstr += String(mac[2], HEX);
  macstr += ":";
  macstr += String(mac[1], HEX);
  macstr += ":";
  macstr += String(mac[0], HEX);
  strcpy(device.name, device_name);
  strcpy(device.mac, macstr.c_str());
  strcpy(device.ip, localIp.c_str());
  strcpy(device.description, device_description);
}

void initComponents() {
  for (int i = 0; i < COMPONENTS_SIZE; i++) {
    components[i].deviceID = device.id;
    char buffer[20];
    sprintf(buffer, "Component %d", i);
    strcpy(components[i].name, buffer);
    strcpy(components[i].description, "description");
    components[i].pin = D8;
    pinMode(D8, OUTPUT);
  }
}

void initProperties() {
  for (int i = 0; i < COMPONENTS_SIZE; i++) {
    properties[i].componentID = components[i].id;
    char buffer[20];
    sprintf(buffer, "Property %d", i);
    strcpy(properties[i].name, buffer);
    strcpy(properties[i].mode, "RW");
    strcpy(properties[i].value, "0");
  }
}

/*
   DEVICES HANDLER
*/

bool addDevice(WiFiClient client) {

  if (device.id > 0) {
    return false;
  }

  String url = prefix;
  url += "add";
  Serial.print("Requesting URL: ");
  Serial.println(url);
  //
  // Step 1: Reserve memory space
  //
  StaticJsonBuffer<200> jsonBuffer;
  //
  // Step 2: Build object tree in memory
  //
  JsonObject& root = jsonBuffer.createObject();
  root["name"] = device.name;
  root["mac"] = device.mac;
  root["ip"] = device.ip;
  root["description"] = device.description;
  //
  // Step 3: Generate the JSON string
  //
  String buffer;
  root.printTo(buffer);
  String jsonRequest = "[";
  jsonRequest += buffer;
  jsonRequest += "]";
  Serial.println(jsonRequest);
  Serial.println();
  // This will send the request to the server
  postRequest(url, jsonRequest, client);
  if (timeout(client))
    return false;
  // Read all the lines of the reply from server and print them to Serial
  String line = "";
  bool success = false;
  while (client.available()) {
    line = client.readStringUntil('[');
    if (line.indexOf('200 OK') > 0) {
      deviceAdded = success = true;
      Serial.println("device added");
    }
  }
  if (success) {
    char response[MAX_CONTENT_SIZE];
    readResponseContent(response, sizeof(response), line);
    if (parseDeviceData(response, &device)) {
      printDeviceData(&device);
    } else {
      success = false;
    }
  }

  return success;
}

bool removeDevice(WiFiClient client) {
  String url = prefix;
  url += "delete/device/";
  url += device.id;
  Serial.print("Requesting URL: ");
  Serial.println(url);
  // This will send the request to the server
  deleteRequest(url, client);
  if (timeout(client))
    return false;
  // Read all the lines of the reply from server and print them to Serial
  String line = "";
  bool success = false;
  while (client.available()) {
    line = client.readStringUntil('\r');
    if (line.indexOf('200 OK') > 0) {
      deviceAdded = false;
      success = true;
      Serial.println("device removed");
    }
  }
  return success;
}

/*
   COMPONENTS HANDLER
*/

bool addComponent(int index, WiFiClient client) {
  Component comp = components[index];

  if (comp.id > 0) {
    return false;
  }

  String url = prefix;
  url += "add/components";
  Serial.print("Requesting URL: ");
  Serial.println(url);
  //
  // Step 1: Reserve memory space
  //
  StaticJsonBuffer<200> jsonBuffer;
  //
  // Step 2: Build object tree in memory
  //
  JsonObject& root = jsonBuffer.createObject();
  root["deviceID"] = comp.deviceID;
  root["name"] = comp.name;
  root["description"] = comp.description;
  //
  // Step 3: Generate the JSON string
  //
  String buffer;
  root.printTo(buffer);
  String jsonRequest = "[";
  jsonRequest += buffer;
  jsonRequest += "]";
  Serial.println(jsonRequest);
  Serial.println();
  // This will send the request to the server
  postRequest(url, jsonRequest, client);
  if (timeout(client))
    return false;
  // Read all the lines of the reply from server and print them to Serial
  String line = "";
  bool success = false;
  while (client.available()) {
    line = client.readStringUntil('[');
    if (line.indexOf('200 OK') > 0) {
      success = true;
      Serial.println("Component added");
    }
  }
  if (success) {
    char response[MAX_CONTENT_SIZE];
    readResponseContent(response, sizeof(response), line);
    if (parseComponentData(response, &components[index])) {
      printComponentData(&components[index]);
    } else {
      success = false;
    }
  }
  return success;
}

bool removeComponent(int index, WiFiClient client) {
  Component comp = components[index];
  String url = prefix;
  url += "delete/component/";
  url += comp.id;
  Serial.print("Requesting URL: ");
  Serial.println(url);
  // This will send the request to the server
  deleteRequest(url, client);
  if (timeout(client))
    return false;
  // Read all the lines of the reply from server and print them to Serial
  bool success = false;
  String line = "";
  while (client.available()) {
    line = client.readStringUntil('\r');
    if (line.indexOf('200 OK') > 0) {
      components[index].id = 0;
      success = true;
      Serial.println("Component removed");
    }
  }
  return success;
}

/*
  PROPERTIES HANDLER
*/
bool addProperty(int index, WiFiClient client) {
  Property prop = properties[index];

  if (prop.id > 0) {
    return false;
  }

  String url = prefix;
  url += "add/properties";
  Serial.print("Requesting URL: ");
  Serial.println(url);
  //
  // Step 1: Reserve memory space
  //
  StaticJsonBuffer<200> jsonBuffer;
  //
  // Step 2: Build object tree in memory
  //
  JsonObject& root = jsonBuffer.createObject();
  root["componentID"] = prop.componentID;
  root["name"] = prop.name;
  root["mode"] = prop.mode;
  root["value"] = prop.value;
  //
  // Step 3: Generate the JSON string
  //
  String buffer;
  root.printTo(buffer);
  String jsonRequest = "[";
  jsonRequest += buffer;
  jsonRequest += "]";
  Serial.println(jsonRequest);
  Serial.println();
  // This will send the request to the server
  postRequest(url, jsonRequest, client);
  if (timeout(client))
    return false;
  // Read all the lines of the reply from server and print them to Serial
  String line = "";
  bool success = false;
  while (client.available()) {
    line = client.readStringUntil('[');
    if (line.indexOf('200 OK') > 0) {
      success = true;
      Serial.println("Property added");
    }
  }
  if (success) {
    char response[MAX_CONTENT_SIZE];
    readResponseContent(response, sizeof(response), line);
    if (parsePropertyData(response, &properties[index])) {
      printPropertyData(&properties[index]);
    } else {
      success = false;
    }
  }
  return success;
}

bool removeProperty(int index, WiFiClient client) {
  Property prop = properties[index];
  String url = prefix;
  url += "delete/property/";
  url += prop.id;
  Serial.print("Requesting URL: ");
  Serial.println(url);
  // This will send the request to the server
  deleteRequest(url, client);
  if (timeout(client))
    return false;
  // Read all the lines of the reply from server and print them to Serial
  String line = "";
  bool success = false;
  while (client.available()) {
    line = client.readStringUntil('\r');
    if (line.indexOf('200 OK') > 0) {
      properties[index].id = 0;
      success = true;
      Serial.println("Property removed");
    }
  }
  return success;
}

bool changeProperty(int propertyID, String value) {
  for (int i = 0; i < (COMPONENTS_SIZE * PROPERTIES_SIZE); i++) {
    Property prop = properties[i];
    if (prop.id == propertyID) {
      strcpy(properties[i].value, value.c_str());
      int val = atoi(value.c_str());
      int compIndex = getComponentIndex(properties[i].componentID);
      if (compIndex == -1) {
        return false;
      }
      Component comp = components[compIndex];
      digitalWrite(comp.pin, val);
      return true;
    }
  }
  return false;
}

char* getProperty(int propertyID) {
  for (int i = 0; i < (COMPONENTS_SIZE * PROPERTIES_SIZE); i++) {
    Property prop = properties[i];
    if (prop.id == propertyID)
      return properties[i].value;
  }
  return " ";
}


// Read the body of the response from the HTTP server
void readResponseContent(char* content, size_t maxSize, String line) {
  line.toCharArray(content, maxSize);
}

bool parseDeviceData(char* content, struct Device* device) {
  const size_t BUFFER_SIZE = JSON_OBJECT_SIZE(6);     // the root object has 5 elements
  StaticJsonBuffer<BUFFER_SIZE> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(content);

  if (!root.success()) {
    Serial.println("JSON parsing failed!");
    return false;
  }

  // Here were copy the strings we're interested in
  device->id = atoi(root["id"]);
  return true;
}

// Print the data extracted from the JSON
void printDeviceData(const struct Device* device) {
  Serial.print("id = ");
  Serial.println(device->id);
  Serial.print("name  = ");
  Serial.println(device->name);
  Serial.print("mac  = ");
  Serial.println(device->mac);
  Serial.print("ip  = ");
  Serial.println(device->ip);
  Serial.print("description  = ");
  Serial.println(device->description);
}

bool parseComponentData(char* content, struct Component* component) {
  const size_t BUFFER_SIZE = JSON_OBJECT_SIZE(4);     // the root object has 5 elements
  StaticJsonBuffer<BUFFER_SIZE> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(content);

  if (!root.success()) {
    Serial.println("JSON parsing failed!");
    return false;
  }

  // Here were copy the strings we're interested in
  component->id = atoi(root["id"]);
  return true;
}

// Print the data extracted from the JSON
void printComponentData(const struct Component* component) {
  Serial.print("id = ");
  Serial.println(component->id);
  Serial.print("deviceID = ");
  Serial.println(component->deviceID);
  Serial.print("name  = ");
  Serial.println(component->name);
  Serial.print("description  = ");
  Serial.println(component->description);
}

bool parsePropertyData(char* content, struct Property* property) {
  const size_t BUFFER_SIZE = JSON_OBJECT_SIZE(5);     // the root object has 5 elements
  StaticJsonBuffer<BUFFER_SIZE> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(content);

  if (!root.success()) {
    Serial.println("JSON parsing failed!");
    return false;
  }

  // Here were copy the strings we're interested in
  property->id = atoi(root["id"]);
  return true;
}

// Print the data extracted from the JSON
void printPropertyData(const struct Property* property) {
  Serial.print("id = ");
  Serial.println(property->id);
  Serial.print("componentID = ");
  Serial.println(property->componentID);
  Serial.print("name  = ");
  Serial.println(property->name);
  Serial.print("mode  = ");
  Serial.println(property->mode);
  Serial.print("value  = ");
  Serial.println(property->value);
}

int getComponentIndex(int componentID) {
  for (int i = 0; i < COMPONENTS_SIZE; i++) {
    Component comp = components[i];
    if (comp.id == componentID) {
      return i;
    }
  }
  return -1;
}

void setup() {
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  WiFi.macAddress(mac);
  localIp = WiFi.localIP().toString();
  initDevice();

  // Populate Multidevices System
  Serial.println();
  Serial.print("connecting to ");
  Serial.println(host);
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 8080;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }
  if (addDevice(client)) {
    initComponents();
    if (addComponent(0, client)) {
      initProperties();
      addProperty(0, client);
    }
  }
  // Start the server
  server.begin();
  Serial.println("Server started");
}

void loop() {
  delay(5000);
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  // Wait until the client sends some data
  Serial.println("new client");
  while (!client.available()) {
    delay(1);
  }

  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();

  // Match the request
  char buffer[50];
  int propertyID;
  int value = 0;
  int startIndex;
  int endIndex;
  String msg;
  if ( (startIndex = req.indexOf("/set/")) != -1) {
    if ( (endIndex = req.indexOf(" HTTP")) != -1) {
      String substr = req.substring(startIndex + 5, endIndex);
      int auxIndex = substr.indexOf("/");
      if (auxIndex != -1) {
        String prop = substr.substring(0, auxIndex);
        String val = substr.substring(auxIndex + 1);
        propertyID = atoi(prop.c_str());
        if (changeProperty(propertyID, val)) {
          msg = "Property ";
          msg += prop;
          msg += " was successfully changed to ";
          msg += val;

          client.flush();

          // Prepare the response
          String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\n";
          s += msg;
          s += "</html>\n";

          // Send the response to the client
          client.print(s);
          delay(1);
          Serial.println("Client disconnected");
		  return;
        }
      }
    } else {
      Serial.println("invalid request");
      client.stop();
      return;
    }
  }
  
  if ( (startIndex = req.indexOf("/get/")) != -1) {
    if ( (endIndex = req.indexOf(" HTTP")) != -1) {
      String prop = req.substring(startIndex + 5, endIndex);
      propertyID = atoi(prop.c_str());
      msg = getProperty(propertyID);
      client.flush();

      // Prepare the response
      String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\n";
      s += msg;
      s += "</html>\n";

      // Send the response to the client
      client.print(s);
      delay(1);
      Serial.println("Client disconnected");
	  return;
    }
  } else {
      Serial.println("invalid request");
      client.stop();
      return;
  }
   
  Serial.println("invalid request");
  client.stop();
  return;


}

