# MultiDevices Controller #


### Motivation ###

Microcontrollers and simple development boards, such Arduino, are not able, out-of-the-box,
to communicate through the internet requiring the addition of complex code to perform
it. In general, these boards have low limited resources making the challenge to add this
functionality harder. One of the most interesting feature is to control these devices from
anywhere and from any device, such a mobile phone, a laptop or a smartwatch, allowing to
expand the possibilities of IoT to several areas, e.g. to the Domotic field.

### Goals ###

This project aims to develop a scalable system to connect and control simple development
boards from any device and any location, through the internet. Thus, the system should be
flexible to cover different usage scenarios, from the simplest ones (with few devices) to the
most complex ones (an entire building, such an hotel).

To achieve that, I propose a plug-and-play system, which is not mandatory to know, a
priori, what are the devices connected nor the user preferences, allowing to add or remove
devices according to user needs.

Also, the system is hardware-independent working with any board, from any brand, with
a minimum set of requirements. This feature allows the users to implement and use with any
devices they need, making them compatible, even if they are proprietary-dependent.
To achieve a proper performance the system consists in a server node, with higher compu-
tation resources and the rules to make devices available online, and a set of low-level devices
that only handle a small amount of data to allow their control.

###Full Report [here](https://bitbucket.org/loading27/multidevicesrest/downloads/multidevices-controller-report.pdf)###