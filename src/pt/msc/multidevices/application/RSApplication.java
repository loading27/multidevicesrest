package pt.msc.multidevices.application;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import pt.msc.multidevices.service.DeviceREST;
import pt.msc.multidevices.service.ClientREST;

@ApplicationPath("/*")
public class RSApplication extends Application {
	public Set<Class<?>> getClasses() {
		Set<Class<?>> s = new HashSet<Class<?>>();
		s.add(ClientREST.class);
		s.add(DeviceREST.class);
		return s;
	}
}
