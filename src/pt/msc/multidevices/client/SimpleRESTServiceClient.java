package pt.msc.multidevices.client;
 
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
 
import org.json.JSONObject;
 
public class SimpleRESTServiceClient {
	public static void main(String[] args) {
		String string = "";
		try {
			System.out.println("======================= Welcome to SimpleRESTServiceClient  =======================");
			System.out.println("Please insert the full path to your JSON input file. (default: C://InputJSON.txt)");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String inputFilePath = br.readLine();
			if(inputFilePath.isEmpty())
				inputFilePath =  "C://InputJSON.txt";
			
			//1: Read file from fileSystem
			InputStream fileInputStream = new FileInputStream(inputFilePath);
			InputStreamReader reader = new InputStreamReader(fileInputStream);
			br = new BufferedReader(reader);
			String line;
			while ((line = br.readLine()) != null) {
				string += line + "\n";
			}
 
			JSONObject jsonObject = new JSONObject(string);
			System.out.println("Input JSON File:" + jsonObject + "\n");
			
			//2: Read ip and service name
			System.out.println("Please insert the ip:port and name of your service. (default: localhost:8080 clientService)");
			br = new BufferedReader(new InputStreamReader(System.in));
			String s = br.readLine();
			String ipPort = "localhost:8080";
			String serviceName = "clientService";
			if(!s.isEmpty()){
				String[] split = s.split(" ");
				if(split.length>0)
					ipPort = split[0];
				if(split.length==2)
					serviceName = split[1];
			}
			
			//3: Now pass JSON File Data to REST Service
			try {
				URL url = new URL("http://"+ipPort+"/MultiDevicesREST/api/"+serviceName);
				URLConnection connection = url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestProperty("Content-Type", "application/json");
				connection.setConnectTimeout(5000);
				connection.setReadTimeout(5000);
				OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
				out.write(jsonObject.toString());
				out.close();
 
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
 
				StringBuffer sb = new StringBuffer();
				String str = in.readLine(); 
				while (str != null) {
					sb.append(str+"\n");
					str = in.readLine();
				}
				System.out.println("\nMultiDevices REST Service:"+serviceName+" Invoked Successfully..");
				System.out.println("\nREST Service response:" + sb.toString());
				in.close();
			} catch (Exception e) {
				System.out.println("\nError while calling MultiDevices REST Service");
				System.out.println(e);
			}
 
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}