package pt.msc.multidevices.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import pt.msc.multidevices.model.Component;

public class ComponentAccess {
	public ArrayList<Component> getComponents(Connection con, int deviceID) throws SQLException {
		String query = "SELECT * FROM components WHERE deviceID = ?";
		ArrayList<Component> componentsList = new ArrayList<Component>();
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setInt(1, deviceID);
		ResultSet rs = stmt.executeQuery();
		try {
			while (rs.next()) {
				Component compObj = new Component();
				compObj.setId(rs.getInt("id"));
				compObj.setDeviceID(rs.getInt("deviceID"));
				compObj.setName(rs.getString("name"));
				compObj.setDescription(rs.getString("description"));
				componentsList.add(compObj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return componentsList;

	}

	public Component getComponent(Connection con, int componentID) throws SQLException {
		String query = "SELECT * FROM components WHERE id = ?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setInt(1, componentID);
		ResultSet rs = stmt.executeQuery();
		Component compObj = new Component();
		try {
			if (rs.next()) {
				compObj.setId(rs.getInt("id"));
				compObj.setDeviceID(rs.getInt("deviceID"));
				compObj.setName(rs.getString("name"));
				compObj.setDescription(rs.getString("description"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return compObj;
	}

	protected ArrayList<Component> addComponents(Connection con, ArrayList<Component> componentsList)
			throws SQLException {
		String query = "INSERT INTO components" + "(deviceID, name, description) VALUES" + "(?,?,?)";
		for (Component component : componentsList) {
			PreparedStatement stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, component.getDeviceID());
			stmt.setString(2, component.getName());
			stmt.setString(3, component.getDescription());
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				component.setId(rs.getInt(1));
			}
		}
		return componentsList;
	}

	protected void removeComponent(Connection con, int componentID) throws SQLException {
		String query = "DELETE FROM components WHERE id = ?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setInt(1, componentID);
		stmt.executeUpdate();
	}

}
