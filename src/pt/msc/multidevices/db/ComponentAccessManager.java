package pt.msc.multidevices.db;

import java.sql.Connection;
import java.util.ArrayList;

import pt.msc.multidevices.model.Component;

public class ComponentAccessManager {

	public ArrayList<Component> getComponents(int deviceID) throws Exception {
		ArrayList<Component> componentsList = new ArrayList<Component>();
		Database db = new Database();
		Connection con = db.getConnection();
		ComponentAccess componentAccess = new ComponentAccess();
		componentsList = componentAccess.getComponents(con, deviceID);
		return componentsList;
	}

	public Component getComponent(int componentID) throws Exception {
		Database db = new Database();
		Connection con = db.getConnection();
		ComponentAccess componentAccess = new ComponentAccess();
		return componentAccess.getComponent(con, componentID);
	}

	public ArrayList<Component> addComponents(ArrayList<Component> componentsList) throws Exception {
		Database db = new Database();
		Connection con = db.getConnection();
		ComponentAccess componentAccess = new ComponentAccess();
		componentsList = componentAccess.addComponents(con, componentsList);
		return componentsList;
	}

	public void removeComponent(int componentID) throws Exception {
		Database db = new Database();
		Connection con = db.getConnection();
		ComponentAccess componentAccess = new ComponentAccess();
		componentAccess.removeComponent(con, componentID);
	}
}