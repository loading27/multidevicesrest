package pt.msc.multidevices.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import pt.msc.multidevices.model.Device;

public class DeviceAccess {
	protected ArrayList<Device> getDevices(Connection con) throws SQLException {
		String query = "SELECT * FROM devices";
		ArrayList<Device> devicesList = new ArrayList<Device>();
		PreparedStatement stmt = con.prepareStatement(query);
		ResultSet rs = stmt.executeQuery();
		try {
			while (rs.next()) {
				Device deviceObj = new Device();
				deviceObj.setId(rs.getInt("id"));
				deviceObj.setName(rs.getString("name"));
				deviceObj.setMac(rs.getString("mac"));
				deviceObj.setIp(rs.getString("ip"));
				deviceObj.setDescription(rs.getString("description"));
				deviceObj.setLastSeen(rs.getTimestamp("lastSeen"));
				devicesList.add(deviceObj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return devicesList;
	}

	protected Device getDevice(Connection con, int deviceID) throws SQLException {
		String query = "SELECT * FROM devices WHERE id = ?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setInt(1, deviceID);
		ResultSet rs = stmt.executeQuery();
		Device deviceObj = new Device();
		try {
			if (rs.next()) {
				deviceObj.setId(rs.getInt("id"));
				deviceObj.setName(rs.getString("name"));
				deviceObj.setMac(rs.getString("mac"));
				deviceObj.setIp(rs.getString("ip"));
				deviceObj.setDescription(rs.getString("description"));
				deviceObj.setLastSeen(rs.getTimestamp("lastSeen"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return deviceObj;
	}
	
	private Device getDevice(Connection con, String mac) throws SQLException {
		String query = "SELECT * FROM devices WHERE mac = ?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setString(1, mac);
		ResultSet rs = stmt.executeQuery();
		Device deviceObj = new Device();
		try {
			if (rs.next()) {
				deviceObj.setId(rs.getInt("id"));
				deviceObj.setName(rs.getString("name"));
				deviceObj.setMac(rs.getString("mac"));
				deviceObj.setIp(rs.getString("ip"));
				deviceObj.setDescription(rs.getString("description"));
				deviceObj.setLastSeen(rs.getTimestamp("lastSeen"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return deviceObj;
	}

	protected ArrayList<Device> addDevices(Connection con, ArrayList<Device> devicesList) throws SQLException {
		String query = "INSERT INTO devices" + "(name, mac, ip, description, lastSeen) VALUES" + "(?,?,?,?,?)";
		for (Device device : devicesList) {
			Device aux = getDevice(con, device.getMac());
			if(aux.getId()!= 0){
				device.setDevice(aux);
				continue;
			}
			PreparedStatement stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, device.getName());
			stmt.setString(2, device.getMac());
			stmt.setString(3, device.getIp());
			stmt.setString(4, device.getDescription());
			device.setLastSeen(new Date());
			stmt.setTimestamp(5, new Timestamp(device.getLastSeen().getTime()));
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				device.setId(rs.getInt(1));
			}
		}
		return devicesList;
	}

	protected void removeDevice(Connection con, int deviceID) throws SQLException {
		String query = "DELETE FROM devices WHERE id = ?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setInt(1, deviceID);
		stmt.executeUpdate();
	}
	
	protected void updateLastSeen(Connection con, int deviceID) throws SQLException {
		String query = "UPDATE devices SET lastSeen = ? WHERE id = ?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setTimestamp(1, new Timestamp(new Date().getTime()));
		stmt.setInt(2, deviceID);
		stmt.executeUpdate();
	}

}
