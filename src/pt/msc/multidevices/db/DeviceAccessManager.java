package pt.msc.multidevices.db;

import java.sql.Connection;
import java.util.ArrayList;

import pt.msc.multidevices.model.Device;

public class DeviceAccessManager {

	public ArrayList<Device> getDevices() throws Exception {
		ArrayList<Device> deviceList = new ArrayList<Device>();
		Database db = new Database();
		Connection con = db.getConnection();
		DeviceAccess deviceAccess = new DeviceAccess();
		deviceList = deviceAccess.getDevices(con);
		return deviceList;
	}

	public Device getDevice(int deviceID) throws Exception {
		Database db = new Database();
		Connection con = db.getConnection();
		DeviceAccess deviceAccess = new DeviceAccess();
		return deviceAccess.getDevice(con, deviceID);
	}

	public ArrayList<Device> addDevices(ArrayList<Device> deviceList) throws Exception {
		Database db = new Database();
		Connection con = db.getConnection();
		DeviceAccess deviceAccess = new DeviceAccess();
		deviceList = deviceAccess.addDevices(con, deviceList);
		return deviceList;
	}

	public void removeDevice(int deviceID) throws Exception {
		Database db = new Database();
		Connection con = db.getConnection();
		DeviceAccess deviceAccess = new DeviceAccess();
		deviceAccess.removeDevice(con, deviceID);
	}

	public void updateLastSeen(int deviceID) throws Exception {
		Database db = new Database();
		Connection con = db.getConnection();
		DeviceAccess deviceAccess = new DeviceAccess();
		deviceAccess.updateLastSeen(con, deviceID);
	}

}