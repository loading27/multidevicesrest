package pt.msc.multidevices.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import pt.msc.multidevices.model.Property;

public class PropertyAccess {
	protected ArrayList<Property> getProperties(Connection con, int componentID) throws SQLException {
		String query = "SELECT * FROM properties WHERE componentID = ?";
		ArrayList<Property> propertiesList = new ArrayList<Property>();
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setInt(1, componentID);
		ResultSet rs = stmt.executeQuery();
		try {
			while (rs.next()) {
				Property prop = new Property();
				prop.setId(rs.getInt("id"));
				prop.setComponentID((rs.getInt("componentID")));
				prop.setName(rs.getString("name"));
				prop.setMode(rs.getString("mode"));
				prop.setValue(rs.getString("value"));
				propertiesList.add(prop);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return propertiesList;
	}

	protected Property getProperty(Connection con, int propertyID) throws SQLException {
		String query = "SELECT * FROM properties WHERE id = ?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setInt(1, propertyID);
		ResultSet rs = stmt.executeQuery();
		Property prop = new Property();
		try {
			if (rs.next()) {
				prop.setId(rs.getInt("id"));
				prop.setComponentID((rs.getInt("componentID")));
				prop.setName(rs.getString("name"));
				prop.setMode(rs.getString("mode"));
				prop.setValue(rs.getString("value"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return prop;
	}

	protected ArrayList<Property> addProperties(Connection con, ArrayList<Property> propertiesList)
			throws SQLException {
		String query = "INSERT INTO properties" + "(componentID, name, mode, value) VALUES" + "(?,?,?,?)";
		for (Property prop : propertiesList) {
			PreparedStatement stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, prop.getComponentID());
			stmt.setString(2, prop.getName());
			stmt.setString(3, prop.getMode().toString());
			stmt.setString(4, prop.getValue());
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				prop.setId(rs.getInt(1));
			}
		}
		return propertiesList;
	}

	protected void removeProperty(Connection con, int propertyID) throws SQLException {
		String query = "DELETE FROM properties WHERE id = ?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setInt(1, propertyID);
		stmt.executeUpdate();
	}

	protected ArrayList<Property> changeProperties(Connection con, ArrayList<Property> propertiesList)
			throws SQLException {
		String query = "UPDATE properties SET value = ? WHERE id = ?";
		for (Property prop : propertiesList) {
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, prop.getValue());
			stmt.setInt(2, prop.getId());
			stmt.executeUpdate();
		}
		return propertiesList;
	}

	protected Property changeProperty(Connection con, Property property) throws SQLException {
		String query = "UPDATE properties SET value = ? WHERE id = ?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setString(1, property.getValue());
		stmt.setInt(2, property.getId());
		stmt.executeUpdate();
		return property;
	}

}
