package pt.msc.multidevices.db;

import java.sql.Connection;
import java.util.ArrayList;

import pt.msc.multidevices.model.ModeEnum;
import pt.msc.multidevices.model.Property;

public class PropertyAccessManager {

	public ArrayList<Property> getProperties(int componentID) throws Exception {
		ArrayList<Property> propertiesList = new ArrayList<Property>();
		Database db = new Database();
		Connection con = db.getConnection();
		PropertyAccess propertyAccess = new PropertyAccess();
		propertiesList = propertyAccess.getProperties(con, componentID);
		return propertiesList;
	}

	public Property getProperty(int propertyID) throws Exception {
		Database db = new Database();
		Connection con = db.getConnection();
		PropertyAccess propertyAccess = new PropertyAccess();
		Property property = propertyAccess.getProperty(con, propertyID);
		return property;
	}

	public ArrayList<Property> addProperties(ArrayList<Property> propertiesList) throws Exception {
		Database db = new Database();
		Connection con = db.getConnection();
		PropertyAccess propertyAccess = new PropertyAccess();
		propertiesList = propertyAccess.addProperties(con, propertiesList);
		return propertiesList;
	}

	public void removeProperty(int propertyID) throws Exception {
		Database db = new Database();
		Connection con = db.getConnection();
		PropertyAccess propertyAccess = new PropertyAccess();
		propertyAccess.removeProperty(con, propertyID);
	}

	public ArrayList<Property> changeProperties(ArrayList<Property> propertiesList) throws Exception {
		Database db = new Database();
		Connection con = db.getConnection();
		PropertyAccess propertyAccess = new PropertyAccess();
		propertiesList = propertyAccess.changeProperties(con, propertiesList);
		return propertiesList;
	}

	public Property changeProperty(Property property) throws Exception {
		Database db = new Database();
		Connection con = db.getConnection();
		PropertyAccess propertyAccess = new PropertyAccess();
		Property propToChange = propertyAccess.getProperty(con, property.getId());

		// Fill Property with all attributes
		property.setComponentID(propToChange.getComponentID());
		property.setMode(propToChange.getMode());
		property.setName(propToChange.getName());
		return propertyAccess.changeProperty(con, property);
	}

	public boolean hasWritePermissions(int propertyID) throws Exception {
		Database db = new Database();
		Connection con = db.getConnection();
		PropertyAccess propertyAccess = new PropertyAccess();
		Property propToTest = propertyAccess.getProperty(con, propertyID);
		if (propToTest.getMode().equalsIgnoreCase(ModeEnum.RO.toString())) {
			return false;
		}
		return true;
	}
}
