package pt.msc.multidevices.model;

public class Component {

	private int id;
	private int deviceID;
	private String name;
	private String description;

	public Component() {
		super();
	}

	public Component(int id, int deviceID, String name, String description) {
		super();
		this.id = id;
		this.deviceID = deviceID;
		this.name = name;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(int deviceID) {
		this.deviceID = deviceID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Component [id=" + id + ", deviceID=" + deviceID + ", name=" + name + ", description=" + description
				+ "]";
	}

}
