package pt.msc.multidevices.model;

import java.util.Date;

public class Device {
	private int id;
	private String name;
	private String mac;
	private String ip;
	private String description;
	private Date lastSeen;

	public Device() {
	}

	public Device(int id, String name, String mac, String ip, String description, Date lastSeen) {
		super();
		this.id = id;
		this.name = name;
		this.mac = mac;
		this.ip = ip;
		this.description = description;
		this.lastSeen = lastSeen;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getLastSeen() {
		return lastSeen;
	}

	public void setLastSeen(Date lastSeen) {
		this.lastSeen = lastSeen;
	}
	
	public void setDevice(Device d){
		this.id = d.getId();
		this.name = d.getName();
		this.mac = d.getMac();
		this.ip = d.getIp();
		this.description = d.getDescription();
		this.lastSeen = d.getLastSeen();
	}

	@Override
	public String toString() {
		return "Device [id=" + id + ", name=" + name + ", mac=" + mac + ", ip=" + ip + ", description=" + description
				+ ", lastSeen=" + lastSeen + "]";
	}

}
