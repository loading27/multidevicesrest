package pt.msc.multidevices.model;

public class Property {
	private int id;
	private int componentID;
	private String name;
	private String mode;
	private String value;

	public Property() {
		super();
	}

	public Property(int id, int componentID, String name, String mode, String value) {
		super();
		this.id = id;
		this.componentID = componentID;
		this.name = name;
		this.mode = mode;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getComponentID() {
		return componentID;
	}

	public void setComponentID(int componentID) {
		this.componentID = componentID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Property [id=" + id + ", componentID=" + componentID + ", name=" + name + ", mode=" + mode + ", value="
				+ value + "]";
	}

}
