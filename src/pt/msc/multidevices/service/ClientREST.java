package pt.msc.multidevices.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import pt.msc.multidevices.db.ComponentAccessManager;
import pt.msc.multidevices.db.DeviceAccessManager;
import pt.msc.multidevices.db.PropertyAccessManager;
import pt.msc.multidevices.model.Component;
import pt.msc.multidevices.model.Device;
import pt.msc.multidevices.model.Property;

@Path("/client")
public class ClientREST {
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verify() {
		System.out.println("verify called");
		String result = "ClientREST Successfully started..";
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}

	@GET
	@Path("/devices")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getConnectedDevices() {
		System.out.println("getConnectedDevices() called");
		String devices = null;
		ArrayList<Device> deviceList = new ArrayList<Device>();
		try {
			deviceList = new DeviceAccessManager().getDevices();
			Gson gson = new Gson();
			devices = gson.toJson(deviceList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (devices != null)
			return Response.status(Status.OK).entity(devices).build();

		return Response.status(Status.NOT_FOUND).build();
	}

	@GET
	@Path("/device/{deviceID}/components")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getDeviceComponents(@PathParam("deviceID") int deviceID) {
		System.out.println("getDeviceComponents(deviceID:" + deviceID + ") called");
		String components = null;
		ArrayList<Component> componentList = new ArrayList<Component>();
		try {
			componentList = new ComponentAccessManager().getComponents(deviceID);
			Gson gson = new Gson();
			components = gson.toJson(componentList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (components != null)
			return Response.status(Status.OK).entity(components).build();

		return Response.status(Status.NOT_FOUND).build();
	}

	@GET
	@Path("/component/{componentID}/properties")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getComponentProperties(@PathParam("componentID") int componentID) {
		System.out.println("getComponentProperties(componentID:" + componentID + ") called");
		String properties = null;
		ArrayList<Property> propertiesList = new ArrayList<Property>();
		try {
			propertiesList = new PropertyAccessManager().getProperties(componentID);
			Gson gson = new Gson();
			properties = gson.toJson(propertiesList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (properties != null)
			return Response.status(Status.OK).entity(properties).build();

		return Response.status(Status.NOT_FOUND).build();
	}

	@GET
	@Path("/property/{propertyID}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getProperty(@PathParam("propertyID") int propertyID) {
		System.out.println("getProperty(propertyID:" + propertyID + ") called");
		String property = null;
		try {
			Property prop = new PropertyAccessManager().getProperty(propertyID);
			Gson gson = new Gson();
			property = gson.toJson(prop);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (property != null)
			return Response.status(Status.OK).entity(property).build();
		return Response.status(Status.NOT_FOUND).build();
	}

	@POST
	@Path("/set/properties")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response changeProperties(InputStream incomingData) {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			System.out.println("changeProperties() called with:\n" + sb.toString());
			JsonArray jsonArray = new JsonParser().parse(sb.toString()).getAsJsonArray();
			in.close();
			Gson gson = new Gson();
			ArrayList<Property> propertiesList = new ArrayList<>();
			for (int i = 0; i < jsonArray.size(); i++) {
				JsonElement str = jsonArray.get(i);
				Property prop = gson.fromJson(str, Property.class);
				propertiesList.add(prop);
			}

			PropertyAccessManager propertyAccessManager = new PropertyAccessManager();
			for (Property prop : propertiesList) {
				if(!propertyAccessManager.hasWritePermissions(prop.getId())){
					return Response.status(Status.BAD_REQUEST).entity("Error setting properties:\n" + ""
							+ "Property " + prop.getId() + " has RO mode").build();
				}
				Property propertyToChange = propertyAccessManager.getProperty(prop.getId());
				int deviceID = new ComponentAccessManager().getComponent(propertyToChange.getComponentID()).getDeviceID();
				Device device = new DeviceAccessManager().getDevice(deviceID);
				String ipDevice = device.getIp();
				if (ipDevice == null || ipDevice.isEmpty()) {
					return Response.status(Status.BAD_REQUEST)
							.entity("Error setting properties:\n" + "No Device found.").build();
				}
				String urlToRead = "http://" + ipDevice + "/set/" + prop.getId() + "/" + prop.getValue();
				StringBuilder result = new StringBuilder();
				URL url = new URL(urlToRead);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setConnectTimeout(5000);
				conn.setReadTimeout(5000);
				int statusCode = conn.getResponseCode();
				BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}
				rd.close();
				System.out.println("\nService response:" + result.toString());
				if(statusCode == Status.OK.getStatusCode()){
					new DeviceAccessManager().updateLastSeen(device.getId());
					propertyAccessManager.changeProperty(prop);
				}
			}
			return Response.status(Status.OK).entity(gson.toJson(propertiesList)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.BAD_REQUEST).entity("Error setting properties:\n" + e.getMessage()).build();
		}
	}

}
