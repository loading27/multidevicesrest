package pt.msc.multidevices.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import pt.msc.multidevices.db.ComponentAccessManager;
import pt.msc.multidevices.db.DeviceAccessManager;
import pt.msc.multidevices.db.PropertyAccessManager;
import pt.msc.multidevices.model.Component;
import pt.msc.multidevices.model.Device;
import pt.msc.multidevices.model.Property;

@Path("/device")
public class DeviceREST {
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verify() {
		System.out.println("verify called");
		String result = "DeviceREST Successfully started..";
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addDevice(InputStream incomingData) {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			System.out.println("addDevice() called with:\n"+sb.toString());
			JsonArray jsonArray = new JsonParser().parse(sb.toString()).getAsJsonArray();
			Gson gson = new Gson();
			ArrayList<Device> devicesList = new ArrayList<>();
			for (int i = 0; i < jsonArray.size(); i++) {
				JsonElement str = jsonArray.get(i);
				Device device = gson.fromJson(str, Device.class);
				devicesList.add(device);
			}
			devicesList = new DeviceAccessManager().addDevices(devicesList);
			return Response.status(Status.OK).entity(gson.toJson(devicesList)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.BAD_REQUEST).entity("Error adding new device:\n" + e.getMessage()).build();
		}
	}

	@DELETE
	@Path("/delete/device/{deviceID}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteDevice(@PathParam("deviceID") int deviceID) {
		System.out.println("deleteDevice(deviceID:"+deviceID+") called");
		try {
			new DeviceAccessManager().removeDevice(deviceID);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.status(Status.OK).build();
	}

	@POST
	@Path("/add/components")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addComponents(InputStream incomingData) {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			System.out.println("addComponents() called with:\n"+sb.toString());
			JsonArray jsonArray = new JsonParser().parse(sb.toString()).getAsJsonArray();
			Gson gson = new Gson();
			ArrayList<Component> componentsList = new ArrayList<>();
			for (int i = 0; i < jsonArray.size(); i++) {
				JsonElement str = jsonArray.get(i);
				Component component = gson.fromJson(str, Component.class);
				componentsList.add(component);
			}
			componentsList = new ComponentAccessManager().addComponents(componentsList);
			return Response.status(Status.OK).entity(gson.toJson(componentsList)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.BAD_REQUEST).entity("Error adding new components:\n" + e.getMessage())
					.build();
		}
	}

	@DELETE
	@Path("/delete/component/{componentID}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteComponent(@PathParam("componentID") int componentID) {
		System.out.println("deleteComponent(componentID:"+componentID+") called");
		try {
			new ComponentAccessManager().removeComponent(componentID);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.status(Status.OK).build();
	}

	@POST
	@Path("/add/properties")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addProperties(InputStream incomingData) {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			System.out.println("addProperties() called with:\n"+sb.toString());
			JsonArray jsonArray = new JsonParser().parse(sb.toString()).getAsJsonArray();
			Gson gson = new Gson();
			ArrayList<Property> propertiesList = new ArrayList<>();
			for (int i = 0; i < jsonArray.size(); i++) {
				JsonElement str = jsonArray.get(i);
				Property property = gson.fromJson(str, Property.class);
				propertiesList.add(property);
				System.out.println("Property added: " + property.toString());
			}
			propertiesList = new PropertyAccessManager().addProperties(propertiesList);
			return Response.status(Status.OK).entity(gson.toJson(propertiesList)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.BAD_REQUEST).entity("Error adding new properties:\n" + e.getMessage())
					.build();
		}
	}

	@DELETE
	@Path("/delete/property/{propertyID}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteProperty(@PathParam("propertyID") int propertyID) {
		System.out.println("deleteProperty(propertyID:"+propertyID+") called");
		try {
			new PropertyAccessManager().removeProperty(propertyID);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.status(Status.OK).build();
	}

	@POST
	@Path("/set/properties")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response changeProperties(InputStream incomingData) {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			System.out.println("changeProperties() called with:\n"+sb.toString());
			JsonArray jsonArray = new JsonParser().parse(sb.toString()).getAsJsonArray();
			Gson gson = new Gson();
			ArrayList<Property> propertiesList = new ArrayList<>();
			for (int i = 0; i < jsonArray.size(); i++) {
				JsonElement str = jsonArray.get(i);
				Property property = gson.fromJson(str, Property.class);
				propertiesList.add(property);
			}
			propertiesList = new PropertyAccessManager().changeProperties(propertiesList);
			return Response.status(Status.OK).entity(gson.toJson(propertiesList)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.BAD_REQUEST).entity("Error adding new properties:\n" + e.getMessage())
					.build();
		}
	}
}
