package pt.msc.multidevices.utils;

import java.sql.Timestamp;
import java.util.Date;

public class DateTransformationUtils {

	public static Timestamp dateToMySQLTimestamp(Date date) {
		return new Timestamp(date.getTime());
	}

}
